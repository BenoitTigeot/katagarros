require 'minitest/autorun'

class KatagarrosTest < Minitest::Unit::TestCase

	def test_J1_gagne
		assert_equal([15,0], katagarros([:j1])[:point])
	end 

	def test_J2_gagne
		assert_equal([0,15], katagarros([:j2])[:point])
	end

	def test_J1_regagne
		assert_equal([30,0], katagarros([:j1,:j1])[:point])
	end 
	def test_J2_egalise
		assert_equal([30,30], katagarros([:j1,:j1,:j2,:j2])[:point])
	end 

  	def test_joli_score
   		skip
   		assert_equal [0, 15], joli({point: [0,1]})
  	end
end


def katagarros(sequence)
  score = {point:[0, 0]}
  sequence.each do |balle|
	if sequence.include?(:j1)
			score[:point][0] += 1
	else
			score[:point][1] += 1
	end
end
	joli(score)
end

def joli(score)
  if score.has_value?([1,0])
  	score = {point:[15, 0]}
  elsif score.has_value?([2,0])
  	score = {point:[30, 0]}
  elsif score.has_value?([3,0])
  	score = {point:[40, 0]}
  elsif score.has_value?([0,1])
  	score = {point:[0, 15]}  
  elsif score.has_value?([0,2])
  	score = {point:[0, 30]}  
  elsif score.has_value?([0,3])
  	score = {point:[0, 40]}  
  else
  	score
  end
end
